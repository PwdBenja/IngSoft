# CLUSTER-ICB

Página web para hacer solicitudes para obtener una cuenta al CLUSTER-ICB de la Escuela de Bioinformática de la Universidad de Talca.


# Instalacion Python3

Es necesario tener Python versión 3.10

# Entorno virtual

Antes de la instalación del entorno virtual, instalemos pip. Pip es un administrador de paquetes que ayuda a instalar, desinstalar y actualizar paquetes para sus proyectos.

```bash
apt-get install python3-venv
```
Creamos el entorno
```bash
python3 -m venv venv=my_env_project
```
Activamos el entorno de trabajo
```bash
source my_env_project/bin/activate
```

# Prerrequisito
En el entorno virtual:
```bash
pip install -r requirements.txt
```
    Package       Version
    ------------- -------
    asgiref         3.5.0
    Django          4.0.4
    numpy           1.22.3
    psycopg2-binary 2.9.3
    sqlparse        0.4.2
    tzdata          2022.1
    pytest          7.1.2

# Instalación  de postgresql

Es necesario instalar postgresql version 14
```bash
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
```
```bash
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
```
```bash
sudo apt-get update
```
```bash
sudo apt-get -y install postgresql-14
```

Se debe crear una base de datos.

```bash
CREATE DATABASE solicitudes;
```

## Configuración de credential.py

Es necesario que en la carpeta ingsoftware exista un archivo llamado credential.py que contendrá las credenciales para conectarse a psql.
```bash
DATABASE_NAME = 'solicitudes'
DATABASE_USERNAME = 'user'
DATABASE_PASSWORD = 'pass'
DATABASE_HOST = '127.0.0.1'
DATABASE_PORT = '5432'
```

## ¿Cómo ejecutar Django?

Estando en la carpeta:
```bash
cd ingsoftware
```
```bash
python3 manage.py migrate
```
```bash
python3 manage.py runserver
```
