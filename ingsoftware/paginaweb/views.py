from django.shortcuts import render, redirect
from .validates import validarRut, limpiarRut, validarRutEnUso, busquedaHash, busquedaUniversidad
from .correo import enviarCorreo, enviarCorreoSolicitante, enviarCorreoAdmin, enviarCredenciales
from .models import createHash, solicitud
from .forms import formTerminos, formularioSolicitud, formularioProfesor, formularioAdmin
from datetime import datetime
from datetime import timedelta

today = datetime.now()


def panela(request):
    data = {"form": formularioAdmin}
    if request.GET.get('hash', False):
        query = busquedaHash(request.GET['hash'])
        data["universidad"] = busquedaUniversidad(query[0]['universidad_id'])
        data["solicitud"] = query[0]
        if request.method == "POST":
            respuestaAdmin = formularioAdmin(data=request.POST)
            if request.POST['botonAdmin'] == 'Enviar':
                if respuestaAdmin.is_valid():
                    respuestaAdminDicc = respuestaAdmin.cleaned_data
                    if enviarCredenciales(request.GET['hash'], respuestaAdminDicc['msg_admin'], today, today+timedelta(days=(query[0]['duracion'])*7)):
                        soli = solicitud.objects.get(hash=request.GET['hash'])
                        soli.estado = "Finalizado"
                        soli.fecha_crea = today
                        soli.fecha_fin = today + timedelta(days=(soli.duracion*7))
                        soli.msg_admin = respuestaAdminDicc['msg_admin']
                        soli.save()
                        data["mensaje_exito"] = "Correo con las credenciales a solicitante se mandó con exito"
                    else:
                        data["mensaje_fallo"] = "Correo con las credenciales a solicitante no se mandó. Intenté nuevamente"
    return render(request, "panela.html", data)


def panel(request):
    # data es el diccionario que contendra los elementos en el html,
    # y el default es el form con el formProfesor
    data = {"form": formularioProfesor}
    # Esto revisa que haya si o si un get hash
    if request.GET.get('hash', False):
        query = busquedaHash(request.GET['hash'])
        # Esto revisa que el hash del get este vinculado a una solicitud
        if not query:
            return redirect('/home/')
        # Se ha encontrado un hash vinculado a una solicitud
        else:
            # guardamos la solicitud en un diccionario
            # para ser mostrado en la view
            data["solicitud"] = query[0]
            # se necesita almacenar el nombre
            # de la universidad para utilizarla
            # dentro de .html
            data["universidad"] = busquedaUniversidad(query[0]['universidad_id'])
            # Ver los diferenes estados de la solicitud
            if query[0]['estado'] == 'En espera':
                # Este post recibe los botones Aceptar y Rechazar
                if request.method == "POST":
                    respuestaProfesor = formularioProfesor(data=request.POST)
                    if request.POST['botonProfesor'] == 'Rechazar':
                        if respuestaProfesor.is_valid():
                            respuestaProfesorDicc = respuestaProfesor.cleaned_data
                            if enviarCorreoSolicitante(query[0], 'rechazado', respuestaProfesorDicc['msg_profesor']):
                                soli = solicitud.objects.get(hash=request.GET['hash'])
                                soli.estado = "Rechazada"
                                soli.msg_profesor = respuestaProfesorDicc['msg_profesor']
                                soli.save()
                                # ENVIAR CORREO ADMIN Y AVISAR A ESTUDIANTE FUE RECHAZADA
                                data['rechazo_exito'] = 'SOLICITUD RECHAZADA CON EXITO'
                            else:
                                data['rechazo_fallido'] = 'Correo no ha podido ser enviado, por favor reintentar'
                    elif request.POST['botonProfesor'] == 'Aceptar':
                        if respuestaProfesor.is_valid():
                            respuestaProfesorDicc = respuestaProfesor.cleaned_data
                            if enviarCorreoSolicitante(query[0], 'aceptado', respuestaProfesorDicc['msg_profesor']):
                                soli = solicitud.objects.get(hash=request.GET['hash'])
                                soli.estado = "Aceptada"
                                soli.msg_profesor = respuestaProfesorDicc['msg_profesor']
                                soli.duracion = respuestaProfesorDicc['duracion']
                                soli.save()
                                data['aceptacion_exitosa'] = 'SOLICITUD ACEPTADA CON EXITO'
                                # COMO PROFESOR ACEPTÓ, SE ENVIA CORREO
                                # ADMIN QUE ENVIE LAS CREDENCIALES!
                                if enviarCorreoAdmin(request.GET['hash']):
                                    data['aceptacion_exitosa'] = data['aceptacion_exitosa'] + '. Se ha enviado un correo al administrador para que haga la cuenta.'
                                else:
                                    data['aceptacion_exitosa'] = data['aceptacion_exitosa'] + " . Pero el correo al administrador no se envió (avisar)."
                            else:
                                data['aceptacion_fallida'] = 'Correo no ha podido ser enviado, por favor reintentar'

            elif query[0]['estado'] == 'Rechazada':
                data["mensaje_rechazada"] = 'SOLICITUD RECHAZADA'
            elif query[0]['estado'] == 'Aceptada':
                data["mensaje_aceptada"] = 'SOLICITUD ACEPTADA'
            else:
                return redirect('/home/')
    else:
        return redirect('/home/')
    return render(request, "panel.html", data)


def terminos(request):
    # Es la forma del formulario terminos
    data = {"form": formTerminos}
    if request.method == "POST":
        # solicitud es igual = HTML, porque data es
        # un diccionario que viene de POST
        solicitud = formTerminos(data=request.POST)
        # Es HTML para ser validado
        if solicitud.is_valid():
            # Se crea una key llamada paso1 refenncia que
            # el paso 1 fue exitoso, y no se puede ingresar
            # por URL si no True
            request.session['paso1'] = True
            # e redirige al siguente paso
            return redirect('/solicitudes/formulario/')
    return render(request, "terminos.html", data)


def formulario(request):
    # Revisar que no se ingreso por URL
    if request.session.get('paso1'):
        # Esta condicion es para cargar un formulario ya hecho,
        #  y se apretó atras en el paso de enviar.html
        if request.session.get('solicitud_atras'):
            # Autorellanado
            data = {"form": formularioSolicitud(
                request.session.get('solicitud_atras'))}
        # Si es la primera vez entra aqui, y el formulario es vacio.
        else:
            data = {"form": formularioSolicitud}
        # Se envió el formulario solicitud
        if request.method == "POST":
            # data es el diccionario de post
            solicitud = formularioSolicitud(data=request.POST)
            # Solicitud es HTML, y por ello se valida.
            if solicitud.is_valid():
                soli = solicitud.cleaned_data
                # Se pone el valor de estado por defecto.
                soli['estado'] = "En espera"
                soli['hash'] = str(createHash())
                # Validación de un Rut correcto
                if validarRut(soli['rut']):
                    soli['rut'] = limpiarRut(soli['rut'])
                    # Se crea key dentro del diccionario soli
                    #  (variable de sesion)
                    # para almacenar la el nombre del objeto universidad
                    soli['universidad_nombre'] = soli['universidad'].nombre
                    # Es necesario reemplazar variable (soli['universidad']),
                    #  ya que genera incompatibilidad con
                    # el json
                    # por enede tirara error si no se elimina, o en este caso,
                    #  se reemplaza por algo (la id)
                    soli['universidad'] = soli['universidad'].id
                    # Entonces, se almacena el diccionario solicitud
                    # en la session
                    request.session['solicitud_form'] = soli
                    if validarRutEnUso(request):
                        # Como todo lo anterior es valido,
                        #  session paso 2 es true.
                        request.session['paso2'] = True
                        return redirect('/solicitudes/enviar/')
                    else:
                        data["form"] = solicitud
                        data["error_rut_label"] = "R.U.T"
                        data["error_rut"] = "RUT ya asociado una solicitud En espera"
                # Esto es cuando esta malo el Rut
                else:
                    data["form"] = solicitud
                    data["error_rut_label"] = "R.U.T"
                    data["error_rut"] = "R.U.T no valido"
            # Cuando el formulario es invalido por una razón
            #  que no es "Rut malo"
            else:
                data["form"] = solicitud
    # Esto es cuando quieren ingresar por URL
    else:
        return redirect('/home/')
    return render(request, "formulario.html", data)


def enviar(request):
    # Verificar que los pasos anteriores se hicieron con exito.
    if request.session.get('paso2'):
        # Diccionario que contendrá el formulario.
        data = {}
        # Recupero la solicitud en forma de diccionario
        formulario = request.session.get('solicitud_form')
        # solicitud diccionario a solicitud HTML
        solicitud = formularioSolicitud(formulario)
        # Guardar diccionario solicitud.
        data['formulario'] = formulario
        if request.method == "POST":
            post = request.POST
            # Se apretó el botón enviar, para ser guardado.
            if post['Enviar'] == 'Enviar':
                # Si la validación es correcta el paso 3 es true
                if solicitud.is_valid():
                    request.session['paso3'] = True
                    return redirect('/solicitudes/exito/')
            # Se apretó el botón atrás y se guarda el formulario en una
            # session, para ser nuevamente autocompletado
            else:
                data = {"form": solicitud}
                request.session['solicitud_atras'] = formulario
                return redirect('/solicitudes/formulario/')
    # Redirigir cuando no estan los pasos anteriores.
    else:
        return redirect('/home/')
    return render(request, "enviar.html", data)


def exito(request):
    data = {}
    if request.session.get('paso3'):
        # Como todo fue un exito, se procede a enviar el correo.
        resultado = enviarCorreo(request)
        if resultado:
            data["mensaje"] = 'Correo enviado con exito!'
            # Recupero la solicitud en forma de diccionario
            formulario = request.session.get('solicitud_form')
            # solicitud diccionario a solicitud HTML
            solicitud = formularioSolicitud(formulario)
            if solicitud.is_valid():
                solicitud.save()
        else:
            data["mensaje"] = "Error correo no enviado! BadHeaderError"
        # Eliminar sessions para que en un proximo formulario
        # se comience de nuevo
        if "paso1" in request.session.keys():
            del request.session["paso1"]
        if "paso2" in request.session.keys():
            del request.session["paso2"]
        if "paso3" in request.session.keys():
            del request.session["paso3"]
        if "solicitud_atras" in request.session.keys():
            del request.session["solicitud_atras"]
    else:
        return redirect('/home/')
    return render(request, "exito.html", data)
