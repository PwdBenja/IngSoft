from django import forms
from .models import solicitud


class formularioAdmin(forms.ModelForm):

    class Meta:
        model = solicitud
        fields = [
            'msg_admin',
            ]

        widgets = {
            'msg_admin': forms.Textarea(attrs={'class': 'form-control'}),
        }

        labels = {
            'msg_admin': 'Mensaje',
        }


class formularioProfesor(forms.ModelForm):
    duracion = forms.IntegerField(required=False)

    class Meta:
        model = solicitud
        fields = [
            'msg_profesor',
            'duracion',
            ]

        widgets = {
            'msg_profesor': forms.Textarea(attrs={'class': 'form-control'}),
            'duracion': forms.NumberInput(attrs={'class': 'form-control'}),
        }

        labels = {
            'msg_profesor': 'Mensaje',
            'duracion': 'Duración',
        }


class formularioSolicitud(forms.ModelForm):

    estado = forms.CharField(widget=forms.HiddenInput(), required=False)
    hash = forms.CharField(widget=forms.HiddenInput(), required=False)

    class Meta:
        model = solicitud
        fields = [
            'rut',
            'nombres',
            'apellidos',
            'correo',
            'universidad',
            'descrip',
            'nombre_prof',
            'correo_prof',
            'estado',
            'hash',
            ]

        widgets = {
            'rut': forms.TextInput(attrs={'class': 'form-control'}),
            'nombres': forms.TextInput(attrs={'class': 'form-control'}),
            'apellidos': forms.TextInput(attrs={'class': 'form-control'}),
            'correo': forms.EmailInput(attrs={'class': 'form-control'}),
            'universidad': forms.Select(attrs={'class': 'form-control'}),
            'descrip': forms.Textarea(attrs={'class': 'form-control'}),
            'nombre_prof': forms.TextInput(attrs={'class': 'form-control'}),
            'correo_prof': forms.EmailInput(attrs={'class': 'form-control'}),
        }

        labels = {
            'rut': 'R.U.T',
            'nombres': 'Nombres',
            'apellidos': 'Apellidos',
            'correo': 'Correo alumno',
            'universidad': 'Universidad',
            'descrip': 'Descripción de labor a realizar',
            'nombre_prof': 'Nombre Profesor/a encargado',
            'correo_prof': 'Correo Profesor/a encargado',
        }


class formTerminos(forms.Form):
    radio_button = forms.BooleanField(label="He leído y acepto los términos y condiciones de uso")
