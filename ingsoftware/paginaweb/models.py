from django.db import models
from tests.test import CustomUserManager
from binascii import hexlify
import os
# Create your models here.


def createHash():
    return str(hexlify(os.urandom(11))).replace("'", "")


class universidad(models.Model):
    nombre = models.TextField()
    objects = CustomUserManager()

    def __str__(self):
        return self.nombre


class solicitud(models.Model):
    rut = models.CharField(max_length=100)
    correo = models.EmailField()
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    universidad = models.ForeignKey(universidad, on_delete=models.RESTRICT)
    descrip = models.TextField()
    nombre_prof = models.CharField(max_length=100)
    correo_prof = models.EmailField()
    estado = models.TextField(null=True)
    fecha_soli = models.DateField(auto_now_add=True)
    msg_profesor = models.TextField(null=True)
    msg_admin = models.TextField(null=True)
    duracion = models.IntegerField(null=True)
    fecha_crea = models.DateField(null=True)
    fecha_fin = models.DateField(null=True)
    hash = models.CharField(max_length=25, unique=True)
    objects = CustomUserManager()

    def __str__(self):
        return self.rut
