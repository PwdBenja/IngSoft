from django.core.mail import send_mail, BadHeaderError
from django.conf import settings

from .models import solicitud
from_email = settings.EMAIL_HOST_USER
correo_admin = 'destruccion117@gmail.com'


def enviarCredenciales(hash, msg_admin, today, venc):
    today = str(today.day)+"/" + str(today.month)+"/" + str(today.year)
    venc = str(venc.day)+"/" + str(venc.month)+"/" + str(venc.year)
    info = solicitud.objects.filter(hash=hash).values()[0]
    nombre = info['nombres'] + " "+info['apellidos']
    id = info['id']
    hash = info['hash']
    duracion = info['duracion']
    correo = info['correo']
    subject = "Solicitud de cuenta para Cluster ICB - ID Solicitud: "+str(id)+" CREDENCIALES"
    message = "Hola " + nombre + " te enviamos las credenciales de la cuenta que solicitantes: \n"
    message = message + msg_admin+" \n"
    message = message + "Tienes la siguente duración (en semanas): "+str(duracion)+"\n"
    message = message + "Podras usar tu cuenta desde el "+str(today)+" hasta el "+str(venc)
    try:
        send_mail(subject, message, from_email, [correo],  fail_silently=False)
        return True
    except BadHeaderError:
        return False


def enviarCorreoAdmin(hash):
    info = solicitud.objects.filter(hash=hash).values()[0]
    nombre = info['nombres'] + " "+info['apellidos']
    id = info['id']
    nom_profesor = info['nombre_prof']
    hash = info['hash']
    duracion = info['duracion']
    msg_profesor = info['msg_profesor']
    subject = "Solicitud de cuenta para Cluster ICB - ID Solicitud: "+str(id)
    message = "Hola administrador. \nLa solicitud de: " + nombre + " fue aceptada por el profesor(a) Titular:"+nom_profesor+"\nEl id de la solicitud es: "+str(id)+".\n"
    message = message + "Por favor enviar credenciales a través del siguente link: \n"
    message = message + "http://127.0.0.1:8000/solicitudes/panela/?hash=" + hash + " \n"
    message = message + "Mensaje de profesor: \n"+msg_profesor+"\nDuración de la cuenta: "+str(duracion)
    try:
        send_mail(subject, message, from_email, [correo_admin],  fail_silently=False)
        return True
    except BadHeaderError:
        return False


def enviarCorreo(request):
    formulario = request.session.get('solicitud_form')
    nombre = formulario['nombres'] + " " + formulario['apellidos']
    correo_prof = formulario['correo_prof']
    hash = formulario['hash']

    subject = "Solicitud de cuenta para Cluster ICB"
    message = "Hola profesor(a) "+formulario["nombre_prof"]+".\nEl estudiante " + nombre + " solicita un cuenta al Cluster ICB teniendolo como profesor Titular \n"
    message = message + "Por favor confimar esta solicitud a través del siguente link: \n"
    message = message + "http://127.0.0.1:8000/solicitudes/panel/?hash=" + hash + "\n\nDescripción del alumno:\n" + formulario['descrip']
    try:
        send_mail(subject, message, from_email, [correo_prof],  fail_silently=False)
        return True
    except BadHeaderError:
        return False


def enviarCorreoSolicitante(query, estado, msg_profesor):
    correo = query['correo']
    subject = "Solicitud de cuenta para Cluster ICB"
    message = "Hola "+query["nombres"]+".\nEl profesor(a)" + query['nombre_prof'] + " ha "+estado+" tu solicitud. \n"
    message = message + "\n" + "Este es el mensaje que dejo el profesor(a):\n" + msg_profesor
    try:
        send_mail(subject, message, from_email, [correo_admin, correo],  fail_silently=False)
        return True
    except BadHeaderError:
        return False
