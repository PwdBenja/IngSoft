from django.urls import path
from paginaweb import views


urlpatterns = [
    path('formulario/', views.formulario, name="formulario"),
    path('terminos/', views.terminos, name="terminos"),
    path('enviar/', views.enviar, name="enviar"),
    path('exito/', views.exito, name="exito"),
    path('panel/', views.panel, name="panel"),
    path('panela/', views.panela, name="panela"),
]
