from itertools import cycle
from .models import solicitud, universidad


def busquedaUniversidad(universidadId):
    info = universidad.objects.filter(id=universidadId).values()
    return info[0]['nombre']


def busquedaHash(hashForm):
    info = solicitud.objects.filter(hash=hashForm).values()
    if len(info) != 0:
        return info
    return False


def validarRutEnUso(request):
    formulario = request.session.get('solicitud_form')
    rutForm = formulario['rut']

    # Se va a buscar en la bd si exite una solicitud con ese RUT
    info = solicitud.objects.filter(rut=rutForm).values()

    # if 'estado' in info[0].keys()
    # Si es !0 quiere decir que hay un solicitudes con este RUT, por ello se
    # verifica si el estado de cada solicitud esta 'En espera' si es asi
    # la solicitud no debe aceptarse (return False)
    if len(info) != 0:
        # Se recorre el array que contiene los diccionarios con
        # el historial de solicitudes asociado a rut
        for i in info:
            if i['estado'] == 'En espera':
                return False
            else:
                return True
    return True
    # else:
    # return True
    # return True


def validarRut(rut):
    caracteres = ['0', '1', '2', '3', '4', '5', '6',
                  '7', '8', '9', 'K', 'k', '-', '.']
    for i in rut:
        for k in caracteres:
            if i == k:
                caracter_valido = True
                break
            else:
                caracter_valido = False
        if not caracter_valido:
            valido = False
            break
        else:
            valido = True
    if not valido:
        return False
    rut = rut.upper()
    rut = rut.replace("-", "")
    rut = rut.replace(".", "")
    aux = rut[:-1]
    dv = rut[-1:]
    revertido = map(int, reversed(str(aux)))
    factors = cycle(range(2, 8))
    s = sum(d * f for d, f in zip(revertido, factors))
    res = (-s) % 11
    if str(res) == dv:
        return True
    elif dv == "K" and res == 10:
        return True
    else:
        return False


def limpiarRut(rut):
    rut = rut.upper()
    rut = rut.replace("-", "")
    rut = rut.replace(".", "")
    return rut
