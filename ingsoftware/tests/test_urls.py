from django.test import SimpleTestCase
from django.urls import reverse, resolve
from paginaweb.views import formulario, panela, panel, terminos, formulario, enviar, exito


class TestUrls(SimpleTestCase):

    def test_formulario_url_resolves(self):
        url = reverse('formulario')
        self.assertEqual(resolve(url).func, formulario)

    def test_terminos_url_resolves(self):
        url = reverse('terminos')
        self.assertEqual(resolve(url).func, terminos)

    def test_envio_url_resolves(self):
        url = reverse('enviar')
        self.assertEqual(resolve(url).func, enviar)

    def test_exito_url_resolves(self):
        url = reverse('exito')
        self.assertEqual(resolve(url).func, exito)

    def test_panel_url_resolves(self):
        url = reverse('panel')
        self.assertEqual(resolve(url).func, panel)

    def test_panela_url_resolves(self):
        url = reverse('panela')
        self.assertEqual(resolve(url).func, panela)
