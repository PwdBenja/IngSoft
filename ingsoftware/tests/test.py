from django.contrib.auth.models import BaseUserManager


class CustomUserManager(BaseUserManager):

    def create_uni(self, nombre):
        uni = self.model(
            nombre=nombre,
            )

        return uni

    def create_soli(self, rut, correo, nombres, apellidos, descrip, nombre_prof, correo_prof, msg_profesor, msg_admin, duracion):

        soli = self.model(
            rut=rut,
            correo=correo,
            nombres=nombres,
            apellidos=apellidos,
            descrip=descrip,
            nombre_prof=nombre_prof,
            correo_prof=correo_prof,
            msg_profesor=msg_profesor,
            msg_admin=msg_admin,
            duracion=duracion)
        return soli
