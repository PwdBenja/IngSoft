from django.test import TestCase
from paginaweb.models import universidad, solicitud


class UniversidadModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        universidad.objects.create(nombre='hola')

    

    def test_name_label(self):
        uni = universidad.objects.get(id=1)
        field_label = uni._meta.get_field('nombre').verbose_name
        self.assertEqual(field_label, 'nombre')
        assert uni.nombre == 'hola'


class SolicitudModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        soli = solicitud.objects.create_soli(
        rut='20',
        correo='20',
        nombres='20',
        apellidos='20',
        descrip='20',
        nombre_prof='20',
        correo_prof='20',
        msg_profesor='20',
        msg_admin='20',
        duracion=20
        )

        assert soli.rut == '20'