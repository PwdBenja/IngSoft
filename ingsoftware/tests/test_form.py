from django.test import SimpleTestCase
from paginaweb.forms import formularioSolicitud, formTerminos, formularioProfesor, formularioAdmin


class formularioSoli(SimpleTestCase):
    def test_expense_form_valid_data(self):
        form = formularioSolicitud(data={'rut': 123,
                                         'nombres': True,
                                         'apellidos': False})
        self.assertFalse(form.is_valid())


class formProfesor(SimpleTestCase):
    def test_expense_form_valid_data(self):
        form = formularioProfesor(data={'msg_profesor': 'asdasdasd',
                                        'duracion': 20})
        self.assertTrue(form.is_valid())

        form = formularioProfesor(data={'msg_profesor': 20,
                                        'duracion': 'asdasdad'})
        self.assertFalse(form.is_valid())


class formAdmin(SimpleTestCase):
    # TextField
    def test_expense_form_valid_data(self):
        form = formularioAdmin(data={'msg_admin': 'asdasdasd'})
        self.assertTrue(form.is_valid())

        form = formularioAdmin(data={'msg_admin': 20})
        self.assertTrue(form.is_valid())


# form.form y no un form.modelform
class formTerminos(SimpleTestCase):
    def test_expense_form_valid_data(self):
        form = formTerminos()
