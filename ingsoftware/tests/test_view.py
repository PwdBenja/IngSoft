from django.test import TestCase, Client
from django.urls import reverse


class TerminosViewTest(TestCase):
    @classmethod
    def setUpTestData(self):
        self.client = Client()

    def test_view_url_terminos_accessible_by_name(self):
        response = self.client.get(reverse('terminos'))
        self.assertEqual(response.status_code, 200)

    def test_view_uses_correct_template_terminos(self):
        response = self.client.get(reverse('terminos'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'terminos.html')


class FormularioViewTest(TestCase):
    @classmethod
    def setUpTestData(self):
        self.client = Client()

    def test_view_url_formulario_accessible_by_name(self):
        response = self.client.get(reverse('formulario'))
        self.assertEqual(response.status_code, 302)

    def test_view_uses_template_formulario(self):
        response = self.client.get(reverse('formulario'))
        self.assertEqual(response.status_code, 302)
        # self.assertTemplateUsed(response, 'formulario.html')


class enviarViewTest(TestCase):
    @classmethod
    def setUpTestData(self):
        self.client = Client()

    def test_view_url_enviar_accessible_by_name(self):
        response = self.client.get(reverse('enviar'))
        self.assertEqual(response.status_code, 302)

    def test_view_uses_correct_template_envio(self):
        response = self.client.get(reverse('enviar'))
        self.assertEqual(response.status_code, 302)
        # self.assertTemplateUsed(response, 'enviar.html')


class exitoViewTest(TestCase):
    @classmethod
    def setUpTestData(self):
        self.client = Client()

    def test_view_url_exito_accessible_by_name(self):
        response = self.client.get(reverse('exito'))
        self.assertEqual(response.status_code, 302)

    def test_view_uses_correct_template_exito(self):
        response = self.client.get(reverse('exito'))
        self.assertEqual(response.status_code, 302)
        # self.assertTemplateUsed(response, 'exito.html')


class panelViewTest(TestCase):
    @classmethod
    def setUpTestData(self):
        self.client = Client()

    def test_view_url_panel_accessible_by_name(self):
        response = self.client.get(reverse('panel'))
        # self.assertEqual(response.status_code, 302)

    def test_view_uses_correct_template_panel(self):
        response = self.client.get(reverse('panel'))
        self.assertEqual(response.status_code, 302)
        # self.assertTemplateUsed(response, 'panel.html')


class panelaViewTest(TestCase):
    @classmethod
    def setUpTestData(self):
        self.client = Client()

    def test_view_url_panela_accessible_by_name(self):
        response = self.client.get(reverse('panela'))
        self.assertEqual(response.status_code, 200)

    def test_view_uses_correct_template_panela(self):
        response = self.client.get(reverse('panela'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'panela.html')
